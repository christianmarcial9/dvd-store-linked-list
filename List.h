#ifndef LIST_H
#define LIST_H
#include <cstddef>
#include <iostream>
#include "linkedList.h"
using namespace std;

class List
{
	public:
		List(){
			head = NULL;
		}
		
		node* addNode(int index, string movieTitle, string movieDirector, string movie Actors, string movieProducer, bool inStock);
		void printList();
	private:
		node* head;
};

#endif